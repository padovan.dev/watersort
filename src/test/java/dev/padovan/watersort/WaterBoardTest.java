package dev.padovan.watersort;

import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class WaterBoardTest {

	private static WaterBoard<Color> waterBoard;

	@BeforeAll
	public static void init() {
		WaterBoardTest.waterBoard = WaterBoard.ofEnum(Color.class,
				new Color[] { Color.PURPLE, Color.LIGHT_BLUE, Color.RED, Color.PURPLE },
				new Color[] { Color.GREY_BLUE, Color.DARK_GREEN, Color.ORANGE, Color.MAGENTA },
				new Color[] { Color.GREEN, Color.GREY_BLUE, Color.BROWN, Color.GREEN },
				new Color[] { Color.BLUE, Color.GREY_BLUE, Color.YELLOW, Color.GREY },
				new Color[] { Color.DARK_GREEN, Color.YELLOW, Color.BROWN, Color.LIGHT_BLUE },
				new Color[] { Color.AQUA, Color.GREY, Color.DARK_GREEN, Color.RED },
				new Color[] { Color.BLUE, Color.AQUA, Color.BLUE, Color.ORANGE },
				new Color[] { Color.NONE, Color.NONE, Color.NONE, Color.NONE },
				new Color[] { Color.PURPLE, Color.DARK_GREEN, Color.GREY, Color.YELLOW },
				new Color[] { Color.PINK, Color.GREEN, Color.BROWN, Color.AQUA },
				new Color[] { Color.BROWN, Color.MAGENTA, Color.RED, Color.PINK },
				new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.PINK },
				new Color[] { Color.MAGENTA, Color.MAGENTA, Color.YELLOW, Color.LIGHT_BLUE },
				new Color[] { Color.GREY_BLUE, Color.GREY, Color.PURPLE, Color.LIGHT_BLUE },
				new Color[] { Color.ORANGE, Color.AQUA, Color.PINK, Color.ORANGE },
				new Color[] { Color.NONE, Color.NONE, Color.NONE, Color.NONE });
	}

	@Test
	public void fromStreamTest() throws IOException {
		final WaterBoard<Color> w = WaterBoard.fromStream(i -> Color.values()[i],
				Color::ordinal,
				Color::name,
				"\\|",
				this.getClass().getResourceAsStream("/input1"));
		Assertions.assertEquals(w.fingerprint(),
				WaterBoardTest.waterBoard.fingerprint());
	}
}
