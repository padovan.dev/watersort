package dev.padovan.watersort;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import dev.padovan.watersort.WaterBoardBase.Pour;

public class WaterBoardBaseTest {

	private static int[] bottle0;
	private static int[] bottle1;
	private static int[] bottle2;
	private static int[] bottle3;
	private static WaterBoardBase wbA;

	@BeforeAll
	public static void init() {
		WaterBoardBaseTest.bottle0 = new int[] { 1, 1, 0, 0 };
		WaterBoardBaseTest.bottle1 = new int[] { 2, 1, 2, 1 };
		WaterBoardBaseTest.bottle2 = new int[] { 0, 0, 0, 0 };
		WaterBoardBaseTest.bottle3 = new int[] { 2, 2, 0, 0 };
		WaterBoardBaseTest.wbA = new WaterBoardBase(
				WaterBoardBaseTest.bottle0,
				WaterBoardBaseTest.bottle1,
				WaterBoardBaseTest.bottle2,
				WaterBoardBaseTest.bottle3);
	}

	@Test
	public void failConstructorInvalidRowCountTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new WaterBoardBase(5, 2,
				new int[] { 0, 1, 1, 1 },
				new int[] { 2, 2, 2, 2 },
				new int[] { 1, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 }));
	}

	@Test
	public void failConstructorInvalidColorTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new WaterBoardBase(
				new int[] { 1, 1, 1, 1 },
				new int[] { 3, 3, 3, 3 },
				new int[] { 0, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 }));
		Assertions.assertThrows(IllegalArgumentException.class, () -> new WaterBoardBase(
				new int[] { 1, 1, 1, 1 },
				new int[] { -1, -1, -1, -1 },
				new int[] { 0, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 }));
	}

	@Test
	public void failConstructorInvalidColorCountTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new WaterBoardBase(
				new int[] { 1, 1, 1, 1 },
				new int[] { 2, 2, 2, 2 },
				new int[] { 1, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 }));
		Assertions.assertThrows(IllegalArgumentException.class, () -> new WaterBoardBase(
				new int[] { 1, 1, 1, 0 },
				new int[] { 2, 2, 2, 2 },
				new int[] { 0, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 }));
	}

	@Test
	public void failConstructorInvalidBottleTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new WaterBoardBase(
				new int[] { 0, 1, 1, 1 },
				new int[] { 2, 2, 2, 2 },
				new int[] { 1, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 }));
	}

	@Test
	public void getBottleNumberTest() {
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getBottleNumber(), 4);
	}

	@Test
	public void getRowPerBottleTest() {
		final WaterBoardBase wb = new WaterBoardBase(3, 2,
				new int[] { 1, 1, 0 },
				new int[] { 2, 2, 2 },
				new int[] { 1, 0, 0 },
				new int[] { 0, 0, 0 });
		Assertions.assertEquals(wb.getRowPerBottle(), 3);
	}

	@Test
	public void getColorTest() {
		for (int i = 0; i < 4; i++) {
			Assertions.assertEquals(WaterBoardBaseTest.wbA.getColor(0, i), WaterBoardBaseTest.bottle0[i]);
			Assertions.assertEquals(WaterBoardBaseTest.wbA.getColor(1, i), WaterBoardBaseTest.bottle1[i]);
			Assertions.assertEquals(WaterBoardBaseTest.wbA.getColor(2, i), WaterBoardBaseTest.bottle2[i]);
			Assertions.assertEquals(WaterBoardBaseTest.wbA.getColor(3, i), WaterBoardBaseTest.bottle3[i]);
		}
	}

	@Test
	public void getTopColorTest() {
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopColor(0), 1);
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopColor(1), 1);
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopColor(2), 0);
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopColor(3), 2);
	}

	@Test
	public void getTopIndexTest() {
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopIndex(0), 1);
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopIndex(1), 3);
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopIndex(2), -1);
		Assertions.assertEquals(WaterBoardBaseTest.wbA.getTopIndex(3), 1);
	}

	@Test
	public void isFullTest() {
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isFull(0));
		Assertions.assertTrue(WaterBoardBaseTest.wbA.isFull(1));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isFull(2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isFull(3));
	}

	@Test
	public void isEmptyTest() {
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isEmpty(0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isEmpty(1));
		Assertions.assertTrue(WaterBoardBaseTest.wbA.isEmpty(2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isEmpty(3));
	}

	@Test
	public void isPourableTest() {
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(0, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(0, 1));
		Assertions.assertTrue(WaterBoardBaseTest.wbA.isPourable(0, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(0, 3));

		Assertions.assertTrue(WaterBoardBaseTest.wbA.isPourable(1, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(1, 1));
		Assertions.assertTrue(WaterBoardBaseTest.wbA.isPourable(1, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(1, 3));

		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(2, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(2, 1));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(2, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(2, 3));

		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(3, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(3, 1));
		Assertions.assertTrue(WaterBoardBaseTest.wbA.isPourable(3, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isPourable(3, 3));
	}

	@Test
	public void isSmartPourableTest() {
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(0, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(0, 1));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(0, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(0, 3));

		Assertions.assertTrue(WaterBoardBaseTest.wbA.isSmartPourable(1, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(1, 1));
		Assertions.assertTrue(WaterBoardBaseTest.wbA.isSmartPourable(1, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(1, 3));

		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(2, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(2, 1));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(2, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(2, 3));

		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(3, 0));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(3, 1));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(3, 2));
		Assertions.assertFalse(WaterBoardBaseTest.wbA.isSmartPourable(3, 3));
	}

	@Test
	public void failPourTest() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < WaterBoardBaseTest.bottle0.length; j++) {
				if (!WaterBoardBaseTest.wbA.isPourable(i, j)) {
					final int bottle = i;
					final int row = j;
					Assertions.assertThrows(RuntimeException.class, () -> WaterBoardBaseTest.wbA.pour(bottle, row));
				}
			}
		}
	}

	@Test
	public void pourTest() {
		final WaterBoardBase wbB = WaterBoardBaseTest.wbA.pour(1, 0);
		Assertions.assertEquals(wbB.getTopColor(0), 1);
		Assertions.assertEquals(wbB.getTopColor(1), 2);
		Assertions.assertEquals(wbB.getTopColor(2), 0);
		Assertions.assertEquals(wbB.getTopColor(3), 2);

		final WaterBoardBase wbB1 = wbB.pour(3, 1);
		Assertions.assertEquals(wbB1.getTopColor(0), 1);
		Assertions.assertEquals(wbB1.getTopColor(1), 2);
		Assertions.assertEquals(wbB1.getTopColor(2), 0);
		Assertions.assertEquals(wbB1.getTopColor(3), 2);

		final WaterBoardBase wbC = WaterBoardBaseTest.wbA.pour(0, 2);
		Assertions.assertEquals(wbC.getTopColor(0), 0);
		Assertions.assertEquals(wbC.getTopColor(1), 1);
		Assertions.assertEquals(wbC.getTopColor(2), 1);
		Assertions.assertEquals(wbC.getTopColor(3), 2);
	}

	@Test
	public void getPourTest() {
		final WaterBoardBase wbB = WaterBoardBaseTest.wbA.pour(1, 0);
		final Pour pourB = wbB.getPour();
		Assertions.assertEquals(pourB.getDeep(), 1);
		Assertions.assertEquals(pourB.getFrom(), 1);
		Assertions.assertEquals(pourB.getTo(), 0);
		Assertions.assertNull(pourB.getPrev());

		final WaterBoardBase wbB1 = wbB.pour(3, 1);
		final Pour pourB1 = wbB1.getPour();
		Assertions.assertEquals(pourB1.getDeep(), 2);
		Assertions.assertEquals(pourB1.getFrom(), 3);
		Assertions.assertEquals(pourB1.getTo(), 1);
		Assertions.assertEquals(pourB1.getPrev().getDeep(), pourB.getDeep());
		Assertions.assertEquals(pourB1.getPrev().getFrom(), pourB.getFrom());
		Assertions.assertEquals(pourB1.getPrev().getTo(), pourB.getTo());

		this.getColorTest(); // test wbA is not modified
		Assertions.assertNull(WaterBoardBaseTest.wbA.getPour());
	}

	@Test
	public void fingerPrintTest() {
		final String fingerprintA = WaterBoardBaseTest.wbA.fingerprint();
		final WaterBoardBase wbB = WaterBoardBaseTest.wbA.pour(0, 2);
		final String fingerprintB = wbB.fingerprint();
		Assertions.assertEquals(fingerprintA, fingerprintB);
		final WaterBoardBase wbC = wbB.pour(1, 2);
		final String fingerprintC = wbC.fingerprint();
		Assertions.assertNotEquals(fingerprintB, fingerprintC);
	}

	@Test
	public void toStringTest() {
		final String string = WaterBoardBaseTest.wbA.toString();
		final String expectedString = """
				| | |1| | | | |\s
				| | |2| | | | |\s
				|1| |1| | | |2|\s
				|1| |2| | | |2|\s
				|=| |=| |=| |=|\s""";

		Assertions.assertEquals(string, expectedString);

		final WaterBoardBase wbB = WaterBoardBaseTest.wbA.pour(1, 0);
		final String pourBString = wbB.getPour().toString();
		final WaterBoardBase wbC = wbB.pour(3, 1);
		final String pourCString = wbC.getPour().toString();
		Assertions.assertTrue(pourCString.contains(pourBString));
	}

	@Test
	public void finishTest() {
		final WaterBoardBase wbFinish = new WaterBoardBase(
				new int[] { 1, 1, 1, 1 },
				new int[] { 2, 2, 2, 2 },
				new int[] { 0, 0, 0, 0 },
				new int[] { 0, 0, 0, 0 });
		Assertions.assertTrue(wbFinish.isFinish());
		final WaterBoardBase wbNotFinish = new WaterBoardBase(
				new int[] { 1, 1, 0, 0 },
				new int[] { 2, 1, 2, 1 },
				new int[] { 2, 2, 0, 0 },
				new int[] { 0, 0, 0, 0 });
		Assertions.assertFalse(wbNotFinish.isFinish());
	}
}
