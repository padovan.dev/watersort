package dev.padovan.watersort;

enum Color {
	NONE,
	ORANGE,
	PINK,
	AQUA,
	PURPLE,
	RED,
	LIGHT_BLUE,
	MAGENTA,
	DARK_GREEN,
	GREY_BLUE,
	GREEN,
	BROWN,
	GREY,
	YELLOW,
	BLUE

}