package dev.padovan.watersort;

import java.util.Arrays;

/**
 *
 * Waterboard: represent the board game
 *
 * Colors are from 1 to N, 0 is empty
 */
public class WaterBoardBase {

	private final int rowPerBottle;
	private final int extraBottles;
	private final int bottleNumber;
	private Pour pour = null;

	private final int[] state;

	// state index
	// row 3 |3| |7| ... | (bottleNumber - 1) * 4 + 3 |
	// row 2 |2| |6| ... | (bottleNumber - 1) * 4 + 2 |
	// row 1 |1| |5| ... | (bottleNumber - 1) * 4 + 1 |
	// row 0 |0| |4| ... | (bottleNumber - 1) * 4 + 0 |
	// ......|=| |=| ... |============================|
	// bottle|0| |1| ... | bottleNumber - 1 __________|

	/**
	 *
	 * @param rowPerBottle Row per bottle
	 * @param extraBottles Empty bottles at the end of the game
	 * @param bottles      Array of bottle, each bottle must be "rowPerBottle"
	 *                     length, colors from 1 to N, 0 is empty
	 *
	 * @throws IllegalArgumentException if bottles are invalid
	 */
	public WaterBoardBase(final int rowPerBottle, final int extraBottles, final int[]... bottles) {
		final int colors = bottles.length - extraBottles;
		final int[] colorsCount = new int[colors];
		for (int bottleIndex = 0; bottleIndex < bottles.length; bottleIndex++) {
			final int[] bottle = bottles[bottleIndex];
			if (bottle.length != rowPerBottle) {
				throw new IllegalArgumentException("Invalid row count " + bottle.length + " for bottle " + bottleIndex);
			}
			for (int row = 1; row < rowPerBottle; row++) {
				if (bottle[row - 1] == 0 && bottle[row] != 0) {
					throw new IllegalArgumentException("Invalid status for bottle " + bottleIndex);
				}
			}
			for (int row = 0; row < rowPerBottle; row++) {
				final int color = bottle[row];
				if (color < 0 || color > colors) {
					throw new IllegalArgumentException("Invalid color " + color);
				}
				if (color > 0) {
					colorsCount[color - 1]++;
				}
			}
		}
		for (int i = 0; i < colorsCount.length; i++) {
			if (colorsCount[i] != rowPerBottle) {
				throw new IllegalArgumentException("Invalid color count " + colorsCount[i] + " for color " + (i + 1));
			}
		}
		this.bottleNumber = bottles.length;
		this.rowPerBottle = rowPerBottle;
		this.extraBottles = extraBottles;
		this.state = new int[rowPerBottle * this.bottleNumber];
		for (int bottleIndex = 0; bottleIndex < this.bottleNumber; bottleIndex++) {
			for (int row = 0; row < rowPerBottle; row++) {
				this.state[row + bottleIndex * rowPerBottle] = bottles[bottleIndex][row];
			}
		}
	}

	/**
	 * Create a WaterBoard with 4 row per bottle and 2 extra bottle
	 *
	 * @param bottles Array of bottle, each bottle must be "rowPerBottle"
	 *                length, colors from 1 to N, 0 is empty
	 *
	 * @throws IllegalArgumentException if bottles are invalid
	 */
	public WaterBoardBase(final int[]... bottles) throws IllegalArgumentException {
		this(4, 2, bottles);
	}

	/**
	 * Inner constructor to clone a waterboard before pour
	 *
	 * @param state
	 */
	private WaterBoardBase(final WaterBoardBase state) {
		this.bottleNumber = state.bottleNumber;
		this.extraBottles = state.extraBottles;
		this.rowPerBottle = state.rowPerBottle;
		this.state = new int[state.state.length];
		System.arraycopy(state.state, 0, this.state, 0, this.state.length);
		this.pour = state.pour;
	}

	/**
	 *
	 * @return the bottle number
	 */
	public int getBottleNumber() {
		return this.bottleNumber;
	}

	/**
	 *
	 * @return the number of row per bottle
	 */
	public int getRowPerBottle() {
		return rowPerBottle;
	}

	/**
	 * @param bottle
	 * @param row
	 * @return color in specific position
	 */
	public int getColor(final int bottle, final int row) {
		return this.state[bottle * this.rowPerBottle + row];
	}

	/**
	 *
	 * @param bottle
	 * @return the color in the highest position for the specified bottle
	 */
	public int getTopColor(final int bottle) {
		for (int i = this.rowPerBottle - 1; i > 0; i--) {
			final int color = this.getColor(bottle, i);
			if (color != 0) {
				return color;
			}
		}
		return this.getColor(bottle, 0);
	}

	/**
	 *
	 * @param bottle
	 * @return the highest row index not empty, if the bottle is empty return -1
	 */
	public int getTopIndex(final int bottle) {
		for (int i = this.rowPerBottle - 1; i >= 0; i--) {
			final int color = this.getColor(bottle, i);
			if (color != 0) {
				return i;
			}
		}
		return -1;
	}

	/**
	 *
	 * @param bottle
	 * @return true if the bottle is full, false otherwise
	 */
	public boolean isFull(final int bottle) {
		return this.getColor(bottle, this.rowPerBottle - 1) != 0;
	}

	/**
	 *
	 * @param bottle
	 * @return true if the bottle is empty, false otherwise
	 */
	public boolean isEmpty(final int bottle) {
		return this.getColor(bottle, 0) == 0;
	}

	/**
	 * Test if is possible pour bottleFrom into bottleTo
	 *
	 * @param bottleFrom
	 * @param bottleTo
	 * @return
	 */
	public boolean isPourable(final int bottleFrom, final int bottleTo) {
		if (bottleFrom == bottleTo) {
			return false;
		}
		if (this.isFull(bottleTo) || this.isEmpty(bottleFrom)) {
			return false;
		}
		if (this.isEmpty(bottleTo)) {
			return true;
		}
		return this.getTopColor(bottleFrom) == this.getTopColor(bottleTo);
	}

	/**
	 * Test if is possible pour bottleFrom into bottleTo with some optimization:
	 *
	 * if a single color bottle pour into an empty bottle return false because the
	 * effective board not change
	 *
	 * @param bottleFrom
	 * @param bottleTo
	 * @return
	 */
	public boolean isSmartPourable(final int bottleFrom, final int bottleTo) {
		if (!this.isPourable(bottleFrom, bottleTo)) {
			return false;
		}
		if (this.isEmpty(bottleTo)) {
			final int fromColor0 = this.getColor(bottleFrom, 0);
			boolean sameColors = true;
			for (int i = 1; i < this.rowPerBottle; i++) {
				final int fromColor = this.getColor(bottleFrom, i);
				if (fromColor == 0) {
					break;
				}
				if (fromColor != fromColor0) {
					sameColors = false;
					break;
				}
			}
			if (sameColors) {
				return false;
			}
		}
		return true;

	}

	/**
	 * Clone the board and pour the bottle
	 *
	 * @param bottleFrom
	 * @param bottleTo
	 * @return new board
	 *
	 * @throws RuntimeException if is not pourable
	 */
	public WaterBoardBase pour(final int bottleFrom, final int bottleTo) {
		if (!this.isPourable(bottleFrom, bottleTo)) {
			throw new RuntimeException("Can't pour " + bottleFrom + " to " + bottleTo);
		}
		final WaterBoardBase next = new WaterBoardBase(this);
		next.myselfPour(bottleFrom, bottleTo);
		return next;
	}

	/**
	 * Test if the board is finish
	 *
	 * @return
	 */
	public boolean isFinish() {
		for (int bottleIndex = 0; bottleIndex < this.bottleNumber; bottleIndex++) {
			final int color = this.state[bottleIndex * this.rowPerBottle];
			for (int row = 1; row < this.rowPerBottle; row++) {
				if (this.state[row + bottleIndex * this.rowPerBottle] != color) {
					return false;
				}
			}
		}
		return true;
	}

	private void myselfPour(final int bottleFrom, final int bottleTo) {
		boolean stop = false;
		while (!stop) {
			if (this.isFull(bottleTo) || this.isEmpty(bottleFrom)) {
				stop = true;
			} else {
				final int topIndexFrom = this.getTopIndex(bottleFrom);
				final int topIndexTo = this.getTopIndex(bottleTo);

				final int colorFrom = this.getColor(bottleFrom, topIndexFrom);
				final int colorTo = topIndexTo < 0 ? 0 : this.getColor(bottleTo, topIndexTo);
				if (colorFrom == colorTo || colorTo == 0) {
					this.state[bottleFrom * this.rowPerBottle + topIndexFrom] = 0;
					this.state[bottleTo * this.rowPerBottle + topIndexTo + 1] = colorFrom;
				} else {
					stop = true;
				}
			}
		}
		this.pour = new Pour(this.pour, bottleFrom, bottleTo);
	}

	/**
	 * Return the pour chain
	 *
	 * @return
	 */
	public Pour getPour() {
		return this.pour;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		for (int row = this.rowPerBottle - 1; row >= 0; row--) {
			for (int bottle = 0; bottle < this.bottleNumber; bottle++) {
				final int color = this.getColor(bottle, row);
				char c;
				if (color == 0) {
					c = ' ';
				} else {
					c = (char) (color + '0');
				}
				sb.append("|" + c + "| ");
			}
			sb.append("\n");
		}
		for (int bottle = 0; bottle < this.bottleNumber; bottle++) {
			sb.append("|=| ");
		}
		return sb.toString();
	}

	/**
	 * Return a string that represent the state, different bottle orders have same
	 * fingerprint
	 *
	 * @return
	 */
	public String fingerprint() {
		final String[] bottles = new String[this.bottleNumber];
		for (int i = 0; i < this.bottleNumber; i++) {
			final StringBuilder sb = new StringBuilder();
			for (int j = 0; j < this.rowPerBottle; j++) {
				sb.append((char) this.getColor(i, j));
			}
			bottles[i] = sb.toString();
		}
		Arrays.sort(bottles);
		final StringBuilder sb = new StringBuilder();
		for (final String string : bottles) {
			sb.append(string);
		}
		return sb.toString();
	}

	/**
	 *
	 * Pour representation
	 *
	 */
	public class Pour {

		private final Pour prev;
		private final int from;
		private final int to;
		private final int deep;

		private Pour(final Pour prev, final int from, final int to) {
			this.prev = prev;
			this.from = from;
			this.to = to;
			this.deep = prev == null ? 1 : prev.deep + 1;
		}

		@Override
		public String toString() {
			if (this.prev == null) {
				return this.from + " -> " + this.to + "\n";
			}
			return this.prev.toString() + this.from + " -> " + this.to + "\n";
		}

		public Pour getPrev() {
			return this.prev;
		}

		public int getFrom() {
			return this.from;
		}

		public int getTo() {
			return this.to;
		}

		/**
		 * Chain lenght
		 *
		 * @return
		 */
		public int getDeep() {
			return this.deep;
		}

	}

}