package dev.padovan.watersort;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Solver<T> {

	private final WaterBoard<T> waterBoard;

	public Solver(final WaterBoard<T> waterBoard) {
		this.waterBoard = waterBoard;
	}

	public WaterBoard<T> solve() {
		return Solver.solve(this.waterBoard, new HashSet<>());
	}

	public WaterBoard<T> best() {
		final HashMap<String, Integer> boardCache = new HashMap<>();
		final AtomicReference<WaterBoard<T>> solution = new AtomicReference<>();
		final AtomicInteger bestSolution = new AtomicInteger(Integer.MAX_VALUE);
		Solver.best(this.waterBoard, boardCache, bestSolution, solution);
		return solution.get();
	}

	private static <T> WaterBoard<T> solve(final WaterBoard<T> s, final Set<String> boardCache) {
		final String sKey = s.fingerprint();

		if (boardCache.contains(sKey)) {
			return null;
		}
		boardCache.add(sKey);
		final int bottleNumber = s.getBottleNumber();
		for (int i = 0; i < bottleNumber; i++) {
			for (int j = 0; j < bottleNumber; j++) {
				if (s.isSmartPourable(i, j)) {
					final WaterBoard<T> s1 = s.pour(i, j);
					if (s1.isFinish()) {
						return s1;
					}
					final WaterBoard<T> result = Solver.solve(s1, boardCache);
					if (result != null) {
						return result;
					}
				}
			}
		}
		return null;
	}

	private static <T> void best(final WaterBoard<T> s, final Map<String, Integer> boardCache,
			final AtomicInteger bestSolution,
			final AtomicReference<WaterBoard<T>> solution) {
		final int sStep = s.getPour() == null ? 0 : s.getPour().getDeep();

		if (sStep + 1 >= bestSolution.get()) {
			return;
		}
		final String sKey = s.fingerprint();
		final Integer cached = boardCache.get(sKey);
		if (cached != null) {
			if (cached.intValue() <= sStep) {
				return;
			}
		}
		boardCache.put(sKey, sStep);
		final int bottleNumber = s.getBottleNumber();
		for (int i = 0; i < bottleNumber; i++) {
			for (int j = 0; j < bottleNumber; j++) {
				if (s.isSmartPourable(i, j)) {
					final WaterBoard<T> s1 = s.pour(i, j);
					if (s1.isFinish()) {
						if (s1.getPour().getDeep() < bestSolution.get()) {
							bestSolution.set(s1.getPour().getDeep());
							solution.set(s1);
						}
					} else {
						Solver.best(s1, boardCache, bestSolution, solution);
					}

				}
			}
		}
	}
}
