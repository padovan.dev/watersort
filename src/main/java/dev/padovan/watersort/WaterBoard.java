package dev.padovan.watersort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.ToIntFunction;

import dev.padovan.watersort.WaterBoardBase.Pour;

public class WaterBoard<T> {

	private final WaterBoardBase waterBoard;

	private final IntFunction<T> toObject;
	private final ToIntFunction<T> toColor;
	private final Function<T, String> toString;

	public static <E extends Enum<E>> WaterBoard<E> ofEnum(final Class<E> e,
			@SuppressWarnings("unchecked") final E[]... bottles) {
		return new WaterBoard<>(i -> e.getEnumConstants()[i], Enum::ordinal, Enum::name, bottles);
	}

	public static <T> WaterBoard<T> fromStream(final IntFunction<T> toObject,
			final ToIntFunction<T> toColor,
			final Function<T, String> toString,
			final String separator,
			final InputStream is) throws IOException {

		final List<String[]> lines = new ArrayList<>();
		int bottleCount = 0;
		int empty = 0;
		try (InputStreamReader isReader = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(isReader)) {
			while (reader.ready()) {
				final String line = reader.readLine();
				final String[] b = line.split(separator);
				if (b.length > 0) {
					if (bottleCount == 0) {
						bottleCount = b.length;
					}
					if (bottleCount != b.length) {
						throw new IllegalArgumentException("Lines with different column count");
					}

					lines.add(b);
					for (final String element : b) {
						if (element.isBlank()) {
							empty++;
						}
					}
				}

			}
		}
		final int colorNumber = bottleCount - empty / lines.size();
		final Map<String, T> m = new HashMap<>();
		for (int i = 1; i <= colorNumber; i++) {
			final T object = toObject.apply(i);
			if (object != null) {
				m.put(toString.apply(object), object);
			}
		}
		@SuppressWarnings("unchecked")
		final T[][] bottles = (T[][]) new Object[bottleCount][lines.size()];
		int row = lines.size();
		for (final String[] line : lines) {
			row--;
			for (int bottle = 0; bottle < line.length; bottle++) {
				final String key = line[bottle].trim();
				T value;
				if (key.isEmpty()) {
					value = toObject.apply(0);
				} else {
					final T tmpValue = m.get(key);
					if (tmpValue == null) {
						throw new IllegalArgumentException("Bad value " + key);
					}
					value = tmpValue;
				}
				bottles[bottle][row] = value;
			}
		}

		return new WaterBoard<>(toObject, toColor, toString, bottles);

	}

	@SafeVarargs
	public WaterBoard(final IntFunction<T> toObject,
			final ToIntFunction<T> toColor,
			final Function<T, String> toString,
			final T[]... bottles) {
		this.toObject = toObject;
		this.toColor = toColor;
		this.toString = toString;
		final int[][] intBottles = new int[bottles.length][bottles[0].length];
		for (int bottleIndex = 0; bottleIndex < bottles.length; bottleIndex++) {
			final T[] bottle = bottles[bottleIndex];
			for (int row = 0; row < bottle.length; row++) {
				final T value = bottles[bottleIndex][row];
				intBottles[bottleIndex][row] = toColor.applyAsInt(value);
			}
		}
		this.waterBoard = new WaterBoardBase(intBottles);
	}

	public WaterBoard(final WaterBoardBase waterBoard,
			final IntFunction<T> toObject,
			final ToIntFunction<T> toColor,
			final Function<T, String> toString) {
		this.waterBoard = waterBoard;
		this.toObject = toObject;
		this.toColor = toColor;
		this.toString = toString;
	}

	public int getBottleNumber() {
		return this.waterBoard.getBottleNumber();
	}

	public int getRowPerBottle() {
		return this.waterBoard.getRowPerBottle();
	}

	public T getColor(final int bottle, final int row) {
		return this.toObject(this.waterBoard.getColor(bottle, row));
	}

	public T getTopColor(final int bottle) {
		return this.toObject(this.waterBoard.getTopColor(bottle));
	}

	public int getTopIndex(final int bottle) {
		return this.waterBoard.getTopIndex(bottle);
	}

	public boolean isFull(final int bottle) {
		return this.waterBoard.isFull(bottle);
	}

	public boolean isEmpty(final int bottle) {
		return this.waterBoard.isEmpty(bottle);
	}

	public boolean isPourable(final int bottleFrom, final int bottleTo) {
		return this.waterBoard.isPourable(bottleFrom, bottleTo);
	}

	public boolean isSmartPourable(final int bottleFrom, final int bottleTo) {
		return this.waterBoard.isSmartPourable(bottleFrom, bottleTo);
	}

	public WaterBoard<T> pour(final int bottleFrom, final int bottleTo) {
		return new WaterBoard<>(this.waterBoard.pour(bottleFrom, bottleTo), this.toObject, this.toColor, this.toString);
	}

	public boolean isFinish() {
		return this.waterBoard.isFinish();
	}

	public Pour getPour() {
		return this.waterBoard.getPour();
	}

	public String fingerprint() {
		return this.waterBoard.fingerprint();
	}

	@Override
	public String toString() {
		final int[] maxLengths = new int[this.getBottleNumber()];
		for (int i = 0; i < maxLengths.length; i++) {
			maxLengths[i] = this.getMaxStringLength(i);
		}
		final StringBuilder sb = new StringBuilder();
		for (int row = this.getRowPerBottle() - 1; row >= 0; row--) {
			for (int bottle = 0; bottle < this.getBottleNumber(); bottle++) {
				final T color = this.getColor(bottle, row);
				final String c = this.center(this.toString(color), maxLengths[bottle]);
				sb.append("|" + c + "| ");
			}
			sb.append("\n");
		}
		for (int bottle = 0; bottle < this.getBottleNumber(); bottle++) {
			sb.append("|");
			this.fill(sb, '=', maxLengths[bottle]);
			sb.append("| ");
		}
		return sb.toString();
	}

	private String toString(final T obj) {
		return obj == null ? "" : this.toString.apply(obj);
	}

	private T toObject(final int color) {
		return color == 0 ? null : this.toObject.apply(color);
	}

	private int getMaxStringLength(final int bottle) {
		final int topIndex = this.getTopIndex(bottle);
		int max = 0;
		for (int i = 0; i <= topIndex; i++) {
			max = Math.max(max, this.toString(this.getColor(bottle, i)).length());
		}
		return max;
	}

	private String center(final String s, final int len) {
		final int sLen = s.length();
		if (sLen >= len) {
			return s;
		}
		final int diff = len - sLen;
		final int left = diff / 2;
		final int right = diff - left;
		final StringBuilder sb = new StringBuilder();
		this.fill(sb, ' ', left);
		sb.append(s);
		this.fill(sb, ' ', right);
		return sb.toString();
	}

	private void fill(final StringBuilder sb, final char c, final int left) {
		for (int i = 0; i < left; i++) {
			sb.append(c);
		}
	}

}
