package dev.padovan.watersort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dev.padovan.watersort.WaterBoardBase.Pour;

public class WaterSort {

	enum Color {
		NONE,
		ORANGE,
		PINK,
		AQUA,
		PURPLE,
		RED,
		LIGHT_BLUE,
		MAGENTA,
		DARK_GREEN,
		GREY_BLUE,
		GREEN,
		BROWN,
		GREY,
		YELLOW,
		BLUE

	}

	public static void main(final String[] args) {

		WaterBoard<Color> w = WaterBoard.ofEnum(Color.class,
				new Color[] { Color.PURPLE, Color.LIGHT_BLUE, Color.RED, Color.PURPLE },
				new Color[] { Color.GREY_BLUE, Color.DARK_GREEN, Color.ORANGE, Color.MAGENTA },
				new Color[] { Color.GREEN, Color.GREY_BLUE, Color.BROWN, Color.GREEN },
				new Color[] { Color.BLUE, Color.GREY_BLUE, Color.YELLOW, Color.GREY },
				new Color[] { Color.DARK_GREEN, Color.YELLOW, Color.BROWN, Color.LIGHT_BLUE },
				new Color[] { Color.AQUA, Color.GREY, Color.DARK_GREEN, Color.RED },
				new Color[] { Color.BLUE, Color.AQUA, Color.BLUE, Color.ORANGE },
				new Color[] { Color.NONE, Color.NONE, Color.NONE, Color.NONE },
				new Color[] { Color.PURPLE, Color.DARK_GREEN, Color.GREY, Color.YELLOW },
				new Color[] { Color.PINK, Color.GREEN, Color.BROWN, Color.AQUA },
				new Color[] { Color.BROWN, Color.MAGENTA, Color.RED, Color.PINK },
				new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.PINK },
				new Color[] { Color.MAGENTA, Color.MAGENTA, Color.YELLOW, Color.LIGHT_BLUE },
				new Color[] { Color.GREY_BLUE, Color.GREY, Color.PURPLE, Color.LIGHT_BLUE },
				new Color[] { Color.ORANGE, Color.AQUA, Color.PINK, Color.ORANGE },
				new Color[] { Color.NONE, Color.NONE, Color.NONE, Color.NONE });

		System.out.println(w.toString());

//		 WaterBoard s = new WaterBoard(4);
//		 s.init(new int[] { 1, 2, 0, 0 }, new int[] { 1, 2, 0, 0 }, new int[] { 0,0,0,0 }, new int[] { 1, 1,2, 2 });
		// System.out.println(s1.pour(1, 3));
		// if (true) {
		// return;
		// }
//		WaterBoard s = new WaterBoard(
//				new int[] { Color.PURPLE.ordinal(), Color.LIGHT_BLUE.ordinal(), Color.RED.ordinal(),
//						Color.PURPLE.ordinal() },
//				new int[] { Color.GREY_BLUE.ordinal(), Color.DARK_GREEN.ordinal(), Color.ORANGE.ordinal(),
//						Color.MAGENTA.ordinal() },
//				new int[] { Color.GREEN.ordinal(), Color.GREY_BLUE.ordinal(), Color.BROWN.ordinal(),
//						Color.GREEN.ordinal() },
//				new int[] { Color.BLUE.ordinal(), Color.GREY_BLUE.ordinal(), Color.YELLOW.ordinal(),
//						Color.GREY.ordinal() },
//				new int[] { Color.DARK_GREEN.ordinal(), Color.YELLOW.ordinal(), Color.BROWN.ordinal(),
//						Color.LIGHT_BLUE.ordinal() },
//				new int[] { Color.AQUA.ordinal(), Color.GREY.ordinal(), Color.DARK_GREEN.ordinal(),
//						Color.RED.ordinal() },
//				new int[] { Color.BLUE.ordinal(), Color.AQUA.ordinal(), Color.BLUE.ordinal(), Color.ORANGE.ordinal() },
//				new int[] { Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal() },
//				new int[] { Color.PURPLE.ordinal(), Color.DARK_GREEN.ordinal(), Color.GREY.ordinal(),
//						Color.YELLOW.ordinal() },
//				new int[] { Color.PINK.ordinal(), Color.GREEN.ordinal(), Color.BROWN.ordinal(), Color.AQUA.ordinal() },
//				new int[] { Color.BROWN.ordinal(), Color.MAGENTA.ordinal(), Color.RED.ordinal(), Color.PINK.ordinal() },
//				new int[] { Color.RED.ordinal(), Color.BLUE.ordinal(), Color.GREEN.ordinal(), Color.PINK.ordinal() },
//				new int[] { Color.MAGENTA.ordinal(), Color.MAGENTA.ordinal(), Color.YELLOW.ordinal(),
//						Color.LIGHT_BLUE.ordinal() },
//				new int[] { Color.GREY_BLUE.ordinal(), Color.GREY.ordinal(), Color.PURPLE.ordinal(),
//						Color.LIGHT_BLUE.ordinal() },
//				new int[] { Color.ORANGE.ordinal(), Color.AQUA.ordinal(), Color.PINK.ordinal(),
//						Color.ORANGE.ordinal() },
//				new int[] { Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal() });
//		System.out.println(s);

//		WaterBoard s = new WaterBoard(16);
//		s.init(new int[] { Color.ORANGE.ordinal(), Color.AQUA.ordinal(), Color.PINK.ordinal(), Color.ORANGE.ordinal() },
//				new int[] { Color.PURPLE.ordinal(), Color.LIGHT_BLUE.ordinal(), Color.RED.ordinal(), Color.PURPLE.ordinal() },
//				new int[] { Color.GREY_BLUE.ordinal(), Color.DARK_GREEN.ordinal(), Color.ORANGE.ordinal(), Color.MAGENTA.ordinal() },
//				new int[] { Color.GREEN.ordinal(), Color.GREY_BLUE.ordinal(), Color.BROWN.ordinal(), Color.GREEN.ordinal() },
//				new int[] { Color.BLUE.ordinal(), Color.GREY_BLUE.ordinal(), Color.YELLOW.ordinal(), Color.GREY.ordinal() },
//				new int[] { Color.DARK_GREEN.ordinal(), Color.YELLOW.ordinal(), Color.BROWN.ordinal(), Color.LIGHT_BLUE.ordinal() },
//				new int[] { Color.BLUE.ordinal(), Color.AQUA.ordinal(), Color.BLUE.ordinal(), Color.ORANGE.ordinal() },
//				new int[] { Color.PURPLE.ordinal(), Color.DARK_GREEN.ordinal(), Color.GREY.ordinal(), Color.YELLOW.ordinal() },
//				new int[] { Color.AQUA.ordinal(), Color.GREY.ordinal(), Color.DARK_GREEN.ordinal(), Color.RED.ordinal() },
//				new int[] { Color.PINK.ordinal(), Color.GREEN.ordinal(), Color.BROWN.ordinal(), Color.AQUA.ordinal() },
//				new int[] { Color.BROWN.ordinal(), Color.MAGENTA.ordinal(), Color.RED.ordinal(), Color.PINK.ordinal() },
//				new int[] { Color.RED.ordinal(), Color.BLUE.ordinal(), Color.GREEN.ordinal(), Color.PINK.ordinal() },
//				new int[] { Color.MAGENTA.ordinal(), Color.MAGENTA.ordinal(), Color.YELLOW.ordinal(), Color.LIGHT_BLUE.ordinal() },
//				new int[] { Color.GREY_BLUE.ordinal(), Color.GREY.ordinal(), Color.PURPLE.ordinal(), Color.LIGHT_BLUE.ordinal() },
//				new int[] { Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal() },
//				new int[] { Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal(), Color.NONE.ordinal() });
//		System.out.println(s);

		// System.out.println(s.pour(1, 2));

		final WaterBoard<Color> solved = new Solver<>(w).best();
		if (solved != null) {
			final List<Pour> l = new ArrayList<>();
			Pour step = solved.getPour();
			while (step != null) {
				l.add(step);
				step = step.getPrev();
			}
			System.out.println(w);
			Collections.reverse(l);
			System.out.println("Solved in " + l.size() + " steps");
			for (final Pour s1 : l) {
				System.out.println(s1.getFrom() + " -> " + s1.getTo());
			}
			for (final Pour s1 : l) {
				WaterBoard<Color> prev = w;
				w = w.pour(s1.getFrom(), s1.getTo());
				System.out.println(toString(prev, w, s1.getFrom(), s1.getTo()));

			}
		}
	}

	private static String toString(final WaterBoard<Color> s, final WaterBoard<Color> s1, final int from,
			final int to) {
		final String[] sString = s.toString().split("\n");
		final String[] s1String = s1.toString().split("\n");
		final StringBuilder sb = new StringBuilder();

		final String p = "  " + from + " -> " + to + "  ";

		for (int i = 0; i < sString.length; i++) {
			sb.append(sString[i]);
			if (i == 0) {
				sb.append(p);
			} else {
				for (int j = 0; j < p.length(); j++) {
					sb.append(" ");
				}
			}

			sb.append(s1String[i] + "\n");
		}
		return sb.toString();
	}
}